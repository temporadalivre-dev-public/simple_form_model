module SimpleFormModel

  class Model

    # Fonte: GoRails Form Objects - https://gorails.com/episodes/form-
    # objects-design-pattern e https://github.com/makandra/active_type e Growing
    # Rails Applicaiton in Practice (PlainObject)
    include ActiveModel::Model

    # Para poder utilizar com Active Model Serilizer.

    # Ler documentação em
    # https://api.rubyonrails.org/classes/ActiveModel/Serialization.html#method-i-serializable_hash,
    # para ver que inclui ActiveModel::Serialization que é da onde vem
    # #serializable_hash. #as_json nada mais é do que invocar
    # serializable_hash com suas opções e admite setar o root (passando root:
    # true ou root: :aguma_coisa)

    # Sem a inclusão deste module, se você invocar #as_json, na verdade estará chamando #as_json
    # no Object#as_json do Rails (confira com
    # .method(:as_json).source_location), que reside em
    # active_support/core_ext/object/json.rb, o qual por sua vez chama
    # .instance_values.as_json(options). Portanto você transforma o objeto em
    # hash (todas as suas instance variables - o method #instance_values fica
    # em active_support/core_ext/object/instance_variables.rb) e depois chama
    # as_json no hash resultante (Hash#as_json, definido no memso core ext),
    # que basicamente chama slice ou except. Portanto as keys que você passar
    # tem que ser strings. 
    # 
    #
    # Já se incluirmos ActiveModel::Serializers::JSON (Ver
    # https://guides.rubyonrails.org/v4.2/active_model_basics.html#serialization),
    # o as_json vai chamar serializable_hash, que depende de um method
    # `attributes` definido na instância com um hash (com keys sendo strings)
    # definido os atributos serializáveis, exemplo:
    #
    # def attributes
    #   {'name' => nil} # ou use `instance_values`, como no exemplo em https://api.rubyonrails.org/classes/ActiveModel/Serializers/JSON.html#method-i-from_json, que gera um hash com key/value, sendo as keys as instance variables sem @.
    # end
    #
    # A vantagem de incluir ActiveModel::Serializers::JSON, é que seu method
    # as_json chama serializable_hash, que admite explicitar chaves de
    # only/except com symbols em adição a strings (na verdade os symbols são
    # convertidas pra string no method, pois instance_values gera hash com
    # keys sendo strings); quanto a geração do hash a ser serializado, na
    # nossa implementação, ambas tem o mesmo efeito pois chamam o method
    # attributes que, no nosso caso, definimos para `instance_values` (mesmo
    # method invocado por Object#as_json).
    include ActiveModel::Serializers::JSON

    # Para o after_save e before_save
    extend ActiveSupport::Callbacks

    # Cria o before_save, around_save, after_save. Fonte: https://api.rubyonrails.org/classes/ActiveModel/Callbacks.html
    define_model_callbacks :save

    # ActiveModel::Validations já é incluído pelo ActiveModel::Model, mas para
    # usar os callbacks (before_validation e after_validation) precisamos
    # incluí-los explicitamente.
    include ActiveModel::Validations::Callbacks

    # Permite definir attributes com typecasting e default, do tipo,
    #  attribute :likes_pineaple, :boolean, default: true
    include ActiveModel::Attributes

    def attributes
      # https://api.rubyonrails.org/classes/Object.html#method-i-instance_values
      instance_values
    end

    # Fonte: Growing Rails Application in Practice, p. 25. 
    def save
      if valid?
        run_callbacks :save do
          true
        end
      else
        false
      end
    end
    
  end

end