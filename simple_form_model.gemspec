$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "simple_form_model/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "simple_form_model"
  spec.version     = SimpleFormModel::VERSION
  spec.authors     = ["TemporadaLivre"]
  spec.email       = ["dev@temporadalivre.com"]
  spec.homepage    = "https://temporadalivre.com"
  spec.summary     = "Summary of SimpleFormModel."
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "http://mygemserver.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails"

  spec.add_development_dependency "sqlite3"
end
